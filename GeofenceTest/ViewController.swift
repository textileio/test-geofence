import UIKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController {

  let locationManager = CLLocationManager()
  let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    return formatter
  }()

  var currentlyMonitoredRegion: CLRegion?

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLocationServices()
    configureNotifications()
  }
}

// MARK: CLLocationManagerDelegate

extension ViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.last else { return }
    if let region = currentlyMonitoredRegion {
      locationManager.stopMonitoring(for: region)
    }
    currentlyMonitoredRegion = CLCircularRegion(center: location.coordinate, radius: 500, identifier: "user_region")
    currentlyMonitoredRegion?.notifyOnExit = true
    currentlyMonitoredRegion?.notifyOnEntry = false
    locationManager.startMonitoring(for: currentlyMonitoredRegion!)
  }

  func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
    let title = "Exited Region"
    let date = dateFormatter.string(from: Date())
    let body = "Getting current location and creating new region."
    displayNotification(title: title, subtitle: date, body: body)
    locationManager.requestLocation()
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    displayNotification(title: "Error", subtitle: "Location Manager", body: error.localizedDescription)
  }
}

// MARK: UNUserNotificationCenterDelegate

extension ViewController: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    completionHandler([.sound, .alert, .badge])
  }
}

// MARK: Private

extension ViewController {
  fileprivate func configureLocationServices() {
    locationManager.delegate = self
    locationManager.allowsBackgroundLocationUpdates = true
    locationManager.pausesLocationUpdatesAutomatically = false
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
    // Request authorization, if needed.
    let authorizationStatus = CLLocationManager.authorizationStatus()
    switch authorizationStatus {
    case .notDetermined:
      // Request authorization.
      locationManager.requestAlwaysAuthorization()
      break
    default:
      break
    }
    locationManager.requestLocation()
  }

  fileprivate func configureNotifications() {
    UNUserNotificationCenter.current().delegate = self
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { _, _ in }
  }

  fileprivate func displayNotification(title: String, subtitle: String, body: String) {
    let content = UNMutableNotificationContent()
    content.title = title
    content.subtitle = subtitle
    content.body = body
    let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil)
    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
  }
}


